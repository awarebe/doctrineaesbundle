<?php

namespace Aware\DoctrineAESBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration tree for security bundle. Full tree you can see in Resources/docs
 *
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Get config from yml file
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder|void
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        // Create tree builder
        $treeBuilder = new TreeBuilder('aware_doctrine_aes');
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // BC layer for symfony/config 4.1 and older
            $rootNode = $treeBuilder->root('aware_doctrine_aes');
        }

        // Grammar of config tree
        $rootNode
            ->children()
            ->scalarNode('encryptor_class')
            ->defaultValue('AES128')
            ->end()
            ->scalarNode('key_directory_path')
            ->defaultValue('%kernel.project_dir%')
            ->end()
            ->end();

        return $treeBuilder;
    }
}