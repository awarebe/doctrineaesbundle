<?php

namespace Aware\DoctrineAESBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

interface DoctrineAESExtensionInterface extends ExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void;

    public function getAlias(): string;
}