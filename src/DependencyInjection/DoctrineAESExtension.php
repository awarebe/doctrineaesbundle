<?php

namespace Aware\DoctrineAESBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Initialization of bundle.
 *
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class DoctrineAESExtension extends Extension implements DoctrineAESExtensionInterface
{
    const SupportedEncryptorClasses = array(
        'AES128' => 'Aware\DoctrineAESBundle\Encryptors\AES128Encryptor',
        'AES192' => 'Aware\DoctrineAESBundle\Encryptors\AES192Encryptor',
        'AES256' => 'Aware\DoctrineAESBundle\Encryptors\AES256Encryptor'
    );

    /**
     * Load in configuration for bundle
     *
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        // Create configuration object
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // If empty encryptor class, use AES128 encryptor
        if (in_array($config['encryptor_class'], array_keys(self::SupportedEncryptorClasses))) {
            $config['encryptor_class_full'] = self::SupportedEncryptorClasses[$config['encryptor_class']];
        } else {
            $config['encryptor_class_full'] = $config['encryptor_class'];
        }

        // Set parameters
        $container->setParameter('aware_doctrine_aes.encryptor_class_name', $config['encryptor_class_full']);
        $container->setParameter('aware_doctrine_aes.key_directory_path',$config['key_directory_path'].'/.'.$config['encryptor_class'].'.key');

        // Load service file
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Get alias for configuration
     *
     * @return string
     */
    public function getAlias(): string
    {
        return 'aware_doctrine_aes';
    }
}