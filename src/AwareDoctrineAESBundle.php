<?php

namespace Aware\DoctrineAESBundle;

use Aware\DoctrineAESBundle\DependencyInjection\DoctrineAESExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AwareDoctrineAESBundle extends Bundle
{
    public function getContainerExtension() :?ExtensionInterface
    {
        return new DoctrineAESExtension();
    }
}