<?php

namespace Aware\DoctrineAESBundle\Subscribers;

use Aware\DoctrineAESBundle\Configuration\Encrypted;
use Aware\DoctrineAESBundle\Encryptors\EncryptorInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * This subscriber checks for fields in entities to encrypt/decrypt
 */
class DoctrineAESSubscriber implements EventSubscriber
{
    /**
     * Appended to end of encrypted value
     */
    const ENCRYPTION_MARKER = '<ENC>';

    /**
     * Encryptor interface namespace
     */
    const ENCRYPTOR_INTERFACE_NS = 'Aware\DoctrineAESBundle\Encryptors\EncryptorInterface';

    /**
     * Encryptor
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Used for restoring the encryptor after changing it
     * @var string
     */
    private $restoreEncryptor;

    /**
     * Count amount of decrypted values in this service
     * @var integer
     */
    public $decryptCounter = 0;

    /**
     * Count amount of encrypted values in this service
     * @var integer
     */
    public $encryptCounter = 0;

    /** @var array */
    private $cachedDecryptions = [];

    /**
     * Initialization of subscriber
     *
     * @param EncryptorInterface|null $encryptor (Optional)  An EncryptorInterface.
     */
    public function __construct(?EncryptorInterface $encryptor)
    {
        $this->encryptor = $encryptor;
        $this->restoreEncryptor = $this->encryptor;
    }

    /**
     * Change the encryptor
     *
     * @param EncryptorInterface|null $encryptor
     */
    public function setEncryptor(?EncryptorInterface $encryptor = null)
    {
        $this->encryptor = $encryptor;
    }

    /**
     * Get the current encryptor
     *
     * @return EncryptorInterface|null returns the encryptor class or null
     */
    public function getEncryptor(): ?EncryptorInterface
    {
        return $this->encryptor;
    }

    /**
     * Restore encryptor to the one set in the constructor.
     */
    public function restoreEncryptor()
    {
        $this->encryptor = $this->restoreEncryptor;
    }

    /**
     * Listen a postUpdate lifecycle event.
     * Decrypt entities property's values when post updated.
     *
     * So for example after form submit the preUpdate encrypted the entity
     * We have to decrypt them before showing them again.
     *
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->processFields($entity, false);
    }

    /**
     * Listen a preUpdate lifecycle event.
     * Encrypt entities property's values on preUpdate, so they will be stored encrypted
     *
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->processFields($entity);
    }

    /**
     * Listen a postLoad lifecycle event.
     * Decrypt entities property's values when loaded into the entity manger
     *
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->processFields($entity, false);
    }

    /**
     * Listen to onflush event
     * Encrypt entities that are inserted into the database
     *
     * @param PreFlushEventArgs $preFlushEventArgs
     */
    public function preFlush(PreFlushEventArgs $preFlushEventArgs)
    {
        $unitOfWOrk = $preFlushEventArgs->getEntityManager()->getUnitOfWork();
        foreach ($unitOfWOrk->getIdentityMap() as $entityName => $entityArray) {
            if (isset($this->cachedDecryptions[$entityName])) {
                foreach ($entityArray as $entityId => $instance) {
                    $this->processFields($instance);
                }
            }
        }
        $this->cachedDecryptions = [];
    }

    /**
     * Listen to onflush event
     * Encrypt entities that are inserted into the database
     *
     * @param OnFlushEventArgs $onFlushEventArgs
     */
    public function onFlush(OnFlushEventArgs $onFlushEventArgs)
    {
        $unitOfWork = $onFlushEventArgs->getEntityManager()->getUnitOfWork();
        foreach ($unitOfWork->getScheduledEntityInsertions() as $entity) {
            $encryptCounterBefore = $this->encryptCounter;
            $this->processFields($entity);
            if ($this->encryptCounter > $encryptCounterBefore ) {
                $classMetadata = $onFlushEventArgs->getEntityManager()->getClassMetadata(get_class($entity));
                $unitOfWork->recomputeSingleEntityChangeSet($classMetadata, $entity);
            }
        }
    }

    /**
     * Listen to postFlush event
     * Decrypt entities after having been inserted into the database
     *
     * @param PostFlushEventArgs $postFlushEventArgs
     */
    public function postFlush(PostFlushEventArgs $postFlushEventArgs)
    {
        $unitOfWork = $postFlushEventArgs->getEntityManager()->getUnitOfWork();
        foreach ($unitOfWork->getIdentityMap() as $entityMap) {
            foreach ($entityMap as $entity) {
                $this->processFields($entity, false);
            }
        }
    }

    /**
     * Realization of EventSubscriber interface method.
     *
     * @return array Return all events which this subscriber is listening
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::postUpdate,
            Events::preUpdate,
            Events::postLoad,
            Events::onFlush,
            Events::preFlush,
            Events::postFlush,
        );
    }

    /**
     * Process (encrypt/decrypt) entities fields
     *
     * @param Object $entity doctrine entity
     * @param Boolean $isEncryptOperation If true - encrypt, false - decrypt entity
     *
     * @return object|null
     *@throws \RuntimeException
     *
     */
    public function processFields(Object $entity, bool $isEncryptOperation = true)
    {
        if (!empty($this->encryptor)) {
            // Check which operation to be used
            $encryptorMethod = $isEncryptOperation ? 'encrypt' : 'decrypt';

            // Get the real class, we don't want to use the proxy classes
            if (strstr(get_class($entity), 'Proxies')) {
                $realClass = ClassUtils::getClass($entity);
            } else {
                $realClass = get_class($entity);
            }

            // Get ReflectionClass of our entity
            $properties = $this->getClassProperties($realClass);

            // Foreach property in the reflection class
            foreach ($properties as $refProperty) {
                if ($this->propertyHasAttribute($refProperty, 'Doctrine\ORM\Mapping\Embedded')) {
                    $this->handleEmbeddedAttribute($entity, $refProperty, $isEncryptOperation);
                    continue;
                }

                /**
                 * If property is an normal value and contains the Encrypt tag, lets encrypt/decrypt that property
                 */
                if ($this->propertyHasAttribute($refProperty, Encrypted::class)) {
                    $pac = PropertyAccess::createPropertyAccessor();
                    $value = $pac->getValue($entity, $refProperty->getName());
                    if ($encryptorMethod == 'decrypt') {
                        if (!is_null($value) and !empty($value)) {
                            if (gettype($value) === "array") {
                                $currentPropArray = array();
                                foreach ($value as $arrIndex => $arrProp) {
                                    if (substr($arrProp, -strlen(self::ENCRYPTION_MARKER)) == self::ENCRYPTION_MARKER) {
                                        $currentPropArray[$arrIndex] = $this->encryptor->decrypt(substr($arrProp, 0, -5));
                                    } else {
                                        $currentPropArray[$arrIndex] = $arrProp;
                                    }
                                }
                                $this->decryptCounter++;
                                $currentPropValue = $currentPropArray;
                                $pac->setValue($entity, $refProperty->getName(), $currentPropValue);
                                $this->cachedDecryptions[$realClass][spl_object_id($entity)][$refProperty->getName()][json_encode($currentPropValue)] = $value;
                            } else {
                                if (substr($value, -strlen(self::ENCRYPTION_MARKER)) == self::ENCRYPTION_MARKER) {
                                    $this->decryptCounter++;
                                    $currentPropValue = $this->encryptor->decrypt(substr($value, 0, -5));
                                    $pac->setValue($entity, $refProperty->getName(), $currentPropValue);
                                    $this->cachedDecryptions[$realClass][spl_object_id($entity)][$refProperty->getName()][$currentPropValue] = $value;
                                }
                            }
                        }
                    } else {
                        if (!is_null($value) and !empty($value)) {
                            if (gettype($value) === "array") {
                                if (isset($this->cachedDecryptions[$realClass][spl_object_id($entity)][$refProperty->getName()][json_encode($value)])) {
                                    $pac->setValue($entity, $refProperty->getName(), $this->cachedDecryptions[$realClass][spl_object_id($entity)][$refProperty->getName()][json_encode($value)]);
                                }
                                $currentPropArray = array();
                                foreach ($value as $arrIndex => $arrProp) {
                                    if (substr($arrProp, -strlen(self::ENCRYPTION_MARKER)) != self::ENCRYPTION_MARKER) {
                                        $currentPropArray[$arrIndex] = $this->encryptor->encrypt($arrProp).self::ENCRYPTION_MARKER;
                                    } else {
                                        $currentPropArray[$arrIndex] = $arrProp;
                                    }
                                }
                                $this->encryptCounter++;
                                $currentPropValue = $currentPropArray;
                                $pac->setValue($entity, $refProperty->getName(), $currentPropValue);
                            } else {
                                if (isset($this->cachedDecryptions[$realClass][spl_object_id($entity)][$refProperty->getName()][$value])) {
                                    $pac->setValue($entity, $refProperty->getName(), $this->cachedDecryptions[$realClass][spl_object_id($entity)][$refProperty->getName()][$value]);
                                }
                                if (substr($value, -strlen(self::ENCRYPTION_MARKER)) != self::ENCRYPTION_MARKER) {
                                    $this->encryptCounter++;
                                    $currentPropValue = $this->encryptor->encrypt($value).self::ENCRYPTION_MARKER;
                                    $pac->setValue($entity, $refProperty->getName(), $currentPropValue);
                                }
                            }
                        }
                    }
                }
            }

            return $entity;
        }

        return $entity;
    }

    /**
     * Handle embedded properties
     *
     * @param $entity
     * @param ReflectionProperty $embeddedProperty
     * @param bool $isEncryptOperation
     */
    private function handleEmbeddedAttribute($entity, ReflectionProperty $embeddedProperty, bool $isEncryptOperation = true)
    {
        $propName = $embeddedProperty->getName();

        $pac = PropertyAccess::createPropertyAccessor();

        $embeddedEntity = $pac->getValue($entity, $propName);

        if ($embeddedEntity) {
            $this->processFields($embeddedEntity, $isEncryptOperation);
        }
    }


    /**
     * Recursive function to get an associative array of class properties
     * including inherited ones from extended classes
     *
     * @param string $className Class name
     *
     * @return array
     */
    private function getClassProperties(string $className): array
    {
        $reflectionClass = new ReflectionClass($className);
        $properties      = $reflectionClass->getProperties();
        $propertiesArray = array();

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            $propertiesArray[$propertyName] = $property;
        }

        if ($parentClass = $reflectionClass->getParentClass()) {
            $parentPropertiesArray = $this->getClassProperties($parentClass->getName());
            if (count($parentPropertiesArray) > 0) {
                $propertiesArray = array_merge($parentPropertiesArray, $propertiesArray);
            }
        }

        return $propertiesArray;
    }

    /**
     * Check is property has attribute
     *
     * @param \ReflectionProperty $property
     * @param string $attribute
     * @return bool
     */
    private function propertyHasAttribute(\ReflectionProperty $property, string $attribute): bool
    {
        //search in array
        foreach ($property->getAttributes() as $propAt) {
            if ($propAt->getName() === $attribute) return true;
        }
        //not found = false
        return false;
    }
}