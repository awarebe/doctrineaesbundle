<?php

namespace Aware\DoctrineAESBundle\Service;

use Aware\DoctrineAESBundle\Subscribers\DoctrineAESSubscriber;

/**
 * Service for manual encryption and decryption
 */
class EncryptionService
{

    /**
     * @var DoctrineAESSubscriber
     */
    private DoctrineAESSubscriber $subscriber;

    /**
     * EncryptionService constructor.
     * @param DoctrineAESSubscriber $subscriber
     */
    public function __construct(DoctrineAESSubscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * Encrypt with for database
     *
     * @param ?string $text
     * @return ?string
     */
    public function encrypt(?string $text): ?string
    {
        // Check if null
        if (null === $text) return null;

        // Get encryptor
        $encryptor = $this->subscriber->getEncryptor();

        // Check if encrypted
        if (str_ends_with($text, '<ENC>') !== false) return $text;

        // Return data
        return $encryptor->encrypt(data: $text) . '<ENC>';
    }

    /**
     * Decrypt
     *
     * @param ?string $text
     * @return ?string
     */
    public function decrypt(?string $text): ?string
    {
        // Check if null
        if (null === $text) return null;

        // Get encryptor
        $encryptor = $this->subscriber->getEncryptor();

        // Check if encrypted
        if (str_ends_with($text, '<ENC>') === false) return $text;

        // Prepare text
        $text = str_replace('<ENC>', '', $text);

        // Return data
        return $encryptor->decrypt(data: $text);
    }
}