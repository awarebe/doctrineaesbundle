<?php

namespace Aware\DoctrineAESBundle\Service;

use Aware\DoctrineAESBundle\Service\EncryptionService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Service for partially searching encrypted fields
 */
class EncryptedSearchService
{
    /**
     * @var EncryptionService
     */
    private EncryptionService $encryptionService;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * EncryptedSearchService constructor
     * @param string $projectDir
     * @param \Aware\DoctrineAESBundle\Service\EncryptionService $encryptionService
     * @param EntityManagerInterface $em
     */
    public function __construct(EncryptionService $encryptionService, EntityManagerInterface $em)
    {
        $this->encryptionService = $encryptionService;
        $this->em = $em;
    }

    /**
     * Get Query element for finding entities on partial encrypted fields
     *
     * @param string $entity Entity name (ex: App:User)
     * @param array $fields Field name in entity
     * @param array $searchValues Values for fields in previous value (same order)
     * @param bool $onlyIds Only return Ids that match, not the entire query
     * @param bool $orMode If one of the fields matches, return
     * @param bool $concatMode Combines fields before search (order is important)
     * @return Query|array
     */
    public function search(string $entity, array $fields, array $searchValues, bool $onlyIds = false, bool $orMode = false, bool $concatMode = false): Query|array
    {
        // Create search file
        $file = $this->createSearchFile($entity, $fields, $concatMode);

        // Create dom crawler
        $crawler = new Crawler($file);

        // Create match string
        $matchString = "//SearchFile/Item[";

        // If concat mode
        if ($concatMode) {
            // Loop fields
            $concatFields = '';
            foreach ($searchValues as $index => $searchValue) {
                $concatFields .= $this->prepareSearchTerm($searchValue);
            }

            // Set in query string
            $matchString .= "contains(@concatField, '" . $concatFields ."')";
        } else {
            // Loop over fields
            $firstVal = true;
            foreach ($fields as $index => $field) {
                if ($firstVal) {
                    $firstVal = false;
                } else {
                    $matchString .= ($orMode ? ' or ' : ' and ');
                }
                $field = str_replace('.', '', $field);
                $matchString .= "contains(@" . $field . ", '" . $this->prepareSearchTerm($searchValues[$index]) . "')";
            }
        }

        // Close match string
        $matchString .= ']';

        // Get elements that match
        $elements = $crawler->filterXPath($matchString)->getIterator();

        // Get resulting ids
        $resIds = [];
        foreach ($elements as $element) {
            $resIds[] = $element->nodeValue;
        }

        // If only ids
        if ($onlyIds) {
            return $resIds;
        }

        // Return
        $result = $this->getEntities($entity, $resIds);
        return $result;
    }

    /**
     * Create file for searching with all entities
     *
     * @param string $entity
     * @param array $fields
     * @param bool $concat
     * @return string
     */
    private function createSearchFile(string $entity, array $fields, bool $concat = false): string
    {
        // Create query builder
        $qb = $this->em->createQueryBuilder();

        // Select string & join info
        $selectString = '';
        $joins = [];
        foreach ($fields as $key => $field) {

            // If field is join, restructure name
            if (str_contains($field, '.')) {

                // Seperate multiple joins
                $joinOrder = explode('.', $field);

                // Get name of field
                $fieldName = array_pop($joinOrder);

                // Loop over joins
                $lastTable = '_item';
                foreach ($joinOrder as $join) {
                    // Construct join info
                    $joinKey = '_' . $join;
                    $joinValue = $lastTable . '.' . $join;
                    $joinArray = [];
                    $joinArray[$joinKey] = $joinValue;

                    // Merge join info
                    $joins = array_merge($joins, $joinArray);

                    // Set name of last table to join
                    $lastTable = '_' . $join;
                }

                // Set field with table
                $fieldName = $lastTable . '.' . $fieldName;

                // Add to select string
                $selectString .= (', ' . $fieldName . ' AS ' . str_replace('.', '', $field));
            }
            // Else just add as regular
            else {
                $selectString .= (', _item.' . $field);
            }
        }

        // Query
        $qb ->select('_item.id' . $selectString)
            ->from($entity, '_item');

        // Joins (if required)
        foreach ($joins as $joinKey => $joinValue) {
            $qb->leftJoin($joinValue, $joinKey);
        }

        // Execute
        $results = $qb ->getQuery()->getScalarResult();

        // Generate search file contents
        $searchFile = new \SimpleXMLElement('<SearchFile />');
        foreach ($results as $result) {
            $item = $searchFile->addChild(
                'Item',
                $result['id']
            );

            // If concat mode
            if ($concat) {
                // Create concat field
                $concatField = '';
                foreach ($fields as $index => $field) {
                    $field = str_replace('.', '', $field);
                    $concatField .= $this->prepareSearchTerm($this->encryptionService->decrypt($result[$field]));
                }

                // Add
                $item->addAttribute('concatField', $concatField);
            } else {
                // Loop over fields
                foreach ($fields as $index => $field) {
                    $field = str_replace('.', '', $field);
                    $item->addAttribute($field, $this->prepareSearchTerm($this->encryptionService->decrypt($result[$field])));
                }
            }
        }

        // Return file
        return $searchFile->asXML();
    }

    /**
     * Get entities based on ids
     *
     * @param string $entity
     * @param array $ids
     * @return Query
     */
    private function getEntities(string $entity, array $ids): Query
    {
        // Create query builder
        $qb = $this->em->createQueryBuilder();

        // Query
        $qb ->select('_item')
            ->from($entity, '_item')
            ->where('_item.id IN (:ids)')
            ->setParameter('ids', $ids);

        // Return query
        return $qb->getQuery();
    }

    /**
     * Strip all special characters from search
     *
     * @param ?string $value
     * @return ?string
     */
    private function prepareSearchTerm(?string $value): ?string
    {
        if (null === $value) return null;
        return strtolower(preg_replace("/[^a-zA-Z0-9]/", "", $value));
    }
}