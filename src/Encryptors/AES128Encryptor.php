<?php

namespace Aware\DoctrineAESBundle\Encryptors;

class AES128Encryptor implements EncryptorInterface
{

    private $encryptionKey;
    private $keyFile;

    /**
     * {@inheritdoc}
     */
    public function __construct(string $keyFile)
    {
        $this->keyFile = $keyFile;
    }

    /**
     * @param string $data Plain text to encrypt
     * @return string Encrypted text
     */
    public function encrypt(string $data): string
    {
        $key = $this->getKey();
        return openssl_encrypt(data: $data, cipher_algo: 'aes-128-ctr', passphrase: $key->Key, options: OPENSSL_ZERO_PADDING, iv: base64_decode($key->Salt));
    }

    /**
     * @param string $data Encrypted text to decrypt
     * @return string Plain text
     */
    public function decrypt(string $data): string
    {
        $key = $this->getKey();
        return openssl_decrypt(data: $data, cipher_algo: 'aes-128-ctr', passphrase: $key->Key, options: OPENSSL_ZERO_PADDING, iv: base64_decode($key->Salt));
    }

    /**
     * Get key for encryption/decryption
     *
     * @return \SimpleXMLElement
     */
    private function getKey(): \SimpleXMLElement
    {
        if ($this->encryptionKey === null) {
            $this->encryptionKey = EncryptorFunctions::getKeyFromFile(keyFile: $this->keyFile, algo: 'aes-128-ctr');
        }

        return $this->encryptionKey;
    }

}