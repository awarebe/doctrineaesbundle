<?php

namespace Aware\DoctrineAESBundle\Encryptors;

/**
 * Mutual functions for encryptor classes
 */
class EncryptorFunctions
{
    /**
     * Get key for encryption
     *
     * @param string $keyFile
     * @param string $algo
     * @return \SimpleXMLElement
     */
    public static function getKeyFromFile(string $keyFile, string $algo): \SimpleXMLElement
    {
        try {
            $key = file_get_contents($keyFile);
            $key = new \SimpleXMLElement($key);
            if ($key === false) throw new \Exception();
        } catch(\Exception $e) {

            //generate salt
            $ivlen = openssl_cipher_iv_length($algo);
            $iv = openssl_random_pseudo_bytes($ivlen);

            //generate key file contents
            $key = new \SimpleXMLElement('<KeyFile />');
            $key->addChild('Key', self::generateRandomString(length: 64));
            $key->addChild('Salt', base64_encode($iv));

            //print in file
            file_put_contents($keyFile, $key->asXML());
        }

        //return
        return $key;
    }

    /**
     * Generate random key of given length
     *
     * @param int $length
     * @return string
     */
    private static function generateRandomString(int $length = 64): string {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!?*-+=';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}