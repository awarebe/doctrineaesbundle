<?php

namespace Aware\DoctrineAESBundle\Encryptors;

/**
 * Interface for all encryptor classes
 */
interface EncryptorInterface
{
    /**
     * @param string $data Plain text to encrypt
     * @return string Encrypted text
     */
    public function encrypt(string $data): string;

    /**
     * @param string $data Encrypted text to decrypt
     * @return string Plain text
     */
    public function decrypt(string $data): string;
}
