<?php

namespace Aware\DoctrineAESBundle\Command;

use Aware\DoctrineAESBundle\Configuration\Encrypted;
use Aware\DoctrineAESBundle\Subscribers\DoctrineAESSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Component\Console\Command\Command;

/**
 * Base command containing usefull base methods.
 **/
class AbstractCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var DoctrineAESSubscriber
     */
    protected DoctrineAESSubscriber $subscriber;

    /**
     * AbstractCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param DoctrineAESSubscriber $subscriber
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        DoctrineAESSubscriber $subscriber
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->subscriber = $subscriber;
    }

    /**
     * Get an result iterator over the whole table of an entity.
     *
     * @param string $entityName
     *
     * @return IterableResult
     */
    protected function getEntityIterator(string $entityName): IterableResult
    {
        $query = $this->entityManager->createQuery(sprintf('SELECT o FROM %s o', $entityName));

        return $query->iterate();
    }

    /**
     * Get the number of rows in an entity-table
     *
     * @param string $entityName
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getTableCount(string $entityName): int
    {
        $query = $this->entityManager->createQuery(sprintf('SELECT COUNT(o) FROM %s o', $entityName));

        return (int) $query->getSingleScalarResult();
    }

    /**
     * Return an array of entity-metadata for all entities
     * that have at least one encrypted property.
     *
     * @return array
     * @throws \ReflectionException
     */
    protected function getEncryptionableEntityMetaData(): array
    {
        $validMetaData = [];
        $metaDataArray = $this->entityManager->getMetadataFactory()->getAllMetadata();

        foreach ($metaDataArray as $entityMetaData)
        {
            if ($entityMetaData instanceof ClassMetadataInfo and $entityMetaData->isMappedSuperclass) {
                continue;
            }

            $properties = $this->getEncryptionableProperties($entityMetaData);
            if (count($properties) == 0) {
                continue;
            }

            $validMetaData[] = $entityMetaData;
        }

        return $validMetaData;
    }

    /**
     * @param $entityMetaData
     *
     * @return array
     * @throws \ReflectionException
     */
    protected function getEncryptionableProperties($entityMetaData): array
    {
        //Create reflectionClass for each meta data object
        $reflectionClass = new \ReflectionClass($entityMetaData->name);
        $propertyArray = $reflectionClass->getProperties();
        $properties    = [];

        foreach ($propertyArray as $property) {
            if ($this->propertyHasAttribute($property, Encrypted::class)) {
                $properties[] = $property;
            }
        }

        return $properties;
    }

    /**
     * Check is property has attribute
     *
     * @param \ReflectionProperty $property
     * @param string $attribute
     * @return bool
     */
    private function propertyHasAttribute(\ReflectionProperty $property, string $attribute): bool
    {
        //search in array
        foreach ($property->getAttributes() as $propAt) {
            if ($propAt->getName() === $attribute) return true;
        }
        //not found = false
        return false;
    }
}