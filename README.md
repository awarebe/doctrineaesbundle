# aware/doctrine-aes-bundle

PHP: >=8
Doctrine: >= 2.9

**Warning**: Doctrine **MUST** be configured to use PHP 8 attributes! (**NOT** annotations)

## Installation

`composer require aware/doctrine-aes-bundle`

## Configuration

### aware_doctrine_aes.yaml

```
aware_doctrine_aes:
    key_directory_path: '%kernel.project_dir%'
    encryptor_class: AES256
```

#### encryptor_class options:

- AES128
- AES192
- AES256

## Usage

### Database

#### Entities
Encrypt a field in an entity with `#[Encrypted]` attribute.        
Use import: `use Aware\DoctrineAESBundle\Configuration\Encrypted;`

#### Commands
Encrypt all marked fields:
```
doctrine:encrypt:database
```
Decrypt all marked fields:
```
doctrine:decrypt:database
```
Check encryption status:
```
doctrine:encrypt:status
```

### Programmatically

#### Encryption Service

The encryption can be done with a service: `use Aware\DoctrineAESBundle\Service\EncryptionService;`

Examples:
```
$enc = $encryptionService->encrypt(text: 'test');
```
```
$dec = $encryptionService->decrypt(text: $enc);
```

#### Encrypted Search Service (SLOW!)

You can search (~MYSQL FIND) on an encrypted field with the service: `use Aware\DoctrineAESBundle\Service\EncryptedSearchService;`

This is slow because it requires all searched fields to temporarily be decrypted (in separate xml file), more rows = longer search time. An instance of ORM Query is returned.

Example:

```
 $query = $encryptedSearchService->search(User::class, ['username', 'lastIp'], ['robbe', '1']);
 $result = $query->getResult();
```

##### Extra options

<table tableid="eb70ed19" columns="2" rows="6" style="--columns:2; --rows:6;" class="">
    <tbody>
        <tr>
            <td class="">$entity (string)</td>
            <td class="">Name of entity</td>
        </tr>
        <tr>
            <td class="">$fields (array)</td>
            <td class="">Array with names of fields to be queried</td>
        </tr>
        <tr>
            <td class="">$searchValues (array)</td>
            <td class="">Array with search string for fields, same order</td>
        </tr>
        <tr>
            <td class="">$onlyIds (bool)</td>
            <td class="">Don't return query but array of id's that match the query</td>
        </tr>
        <tr>
            <td class="">$orMode (bool)</td>
            <td class="">Not every field has to have a match</td>
        </tr>
        <tr>
            <td class="">$concatMode (bool)</td>
            <td class="">All fields and search strings are added together in the order of the array. Usefull for things such as firstname + lastname</td>
        </tr>
    </tbody>
</table>